#include <Audio.h>
#include <Wire.h>
#include <SD.h>
#include <SPI.h>
#include <SerialFlash.h>

#include <Bounce.h>

/* #include "chords.h" */

const int trigPin = 9;
Bounce trig = Bounce(trigPin, 10);

// Special thanks to Matthew Rahtz - http://amid.fish/karplus-strong/

int mode = 0; // sine

// GUItool: begin automatically generated code
AudioSynthWaveform       waveform1;      //xy=188.3333282470703,472.3333435058594
AudioEffectEnvelope      env1;      //xy=605.3333282470703,480.3333282470703
AudioOutputI2S           i2s1;           //xy=862.3333129882812,475.3333435058594
AudioConnection          patchCord1(waveform1, 0, env1, 0);
AudioConnection          patchCord3(env1, 0, i2s1, 0);
AudioConnection          patchCord4(env1, 0, i2s1, 1);
AudioControlSGTL5000     sgtl5000_1;
// GUItool: end automatically generated code


void setup() {
  AudioMemory(15);
  sgtl5000_1.enable();
  sgtl5000_1.volume(0.6);

  waveform1.frequency(110);
  waveform1.amplitude(0.1);
  waveform1.begin(WAVEFORM_SINE);

  env1.attack(10);
  env1.hold(10);
  env1.decay(25);
  env1.sustain(0.5);
  env1.release(70);
  env1.releaseNoteOn(10);

  pinMode(trigPin, INPUT_PULLDOWN);
  delay(700);
}

/* void strum_up(const float *chord, float velocity); */
/* void strum_dn(const float *chord, float velocity); */

void loop() {
  /* const float *chord; */

  int noteIn = analogRead(A0);
  float note = map((float)noteIn, 0, 1023, 20, 100);
  float freq = 440.0f * exp2f((float)(note - 69) * 0.0833333f);

  waveform1.frequency(freq);
  float vel = analogRead(A2)/1023.0f;
  /* Serial.println(noteIn); */

  if (trig.update()){
    if (trig.risingEdge()){
      env1.release(vel*1000);
      Serial.print("note: ");
      Serial.print(note);
      Serial.print("freq: ");
      Serial.println(freq);
      env1.noteOn();
    }
    if (trig.fallingEdge()){
      env1.noteOff();
      /* Serial.println("falling"); */
    }
  }
  /* delay(10); */
}

