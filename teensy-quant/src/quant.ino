
#include <Arduino.h>

static int cv_pin = 0;

int scale[] = {0, 2, 3, 5, 7, 8, 11, 12};
float cv2note_ratio = 9.9f * 12.0f / 8192.0f;

void setup(){
  /* Serial.begin(19200); */

  analogReadResolution(13);
  analogWriteResolution(12);
}

void loop(){
  int cv_in = analogRead(cv_pin);
  /* int note = cv2note(cv_in); */
  float note = (float)cv_in * cv2note_ratio;
  int note_quant = quant(note);

  int cv_out = map(note_quant, 0, 60, 0, 4096);
  cv_out = min(cv_out, 4096);
  analogWrite(26, cv_out);
}

int cv2note(int val){
  float note = (float)val * cv2note_ratio;
  return round(note);
}

int quant(float val){
  int oct = floor(val/12);
  float rest = val - 12*oct;
  for (int i=0; i<8; i++){
    if (rest<scale[i]){
      if (scale[i] - rest < rest-scale[i-1]){
        return scale[i] + oct*12;
      }else{
        return scale[i-1] + oct*12;
      }
    }
  }
  return 0;
}
